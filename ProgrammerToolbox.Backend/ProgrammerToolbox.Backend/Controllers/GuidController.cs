﻿using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace ProgrammerToolbox.Backend.Controllers
{
    [ApiController]
    [Route("guid")]
    public class GuidController : ControllerBase
    {
        [HttpGet]
        public ActionResult Get()
        {
            var guid = Guid.NewGuid().ToString();
            string jsonGuid = JsonSerializer.Serialize(guid);
            return Ok(jsonGuid);
        }
    }
}
